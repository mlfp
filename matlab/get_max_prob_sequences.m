function sequences = select_zero_prob_transitions(model, length, varmap)

for i=1:model.num_mcs
    sequences{i} = get_max_prob_sequence(model.chain(i), length, varmap);
end    