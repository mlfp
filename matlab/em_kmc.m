function [model max_likelihood_clusters] = em_kmc(X, num_mcs)

num_states = size(X.varmap, 1);
% Run 5 random iterations, taking the one w/ the best likelihood
iterations = 1;
best_log_lik = 0;
for i=1:iterations
    tmp_model = init_kmc(num_states, num_mcs);
    [e tmp_model log_lik] = em_kmc_w_model(X, tmp_model);
    if (i == 1 | log_lik > best_log_lik)
        fprintf('Replacing model w/ new model w/ better log_lik\n');
        best_log_lik = log_lik;
        model = tmp_model;
    end
end
max_likelihood_clusters = get_max_clusters(X, model);

% X contains the training sequences, which are not necessarily the same
% length
function [expected_counts model log_lik] = em_kmc_w_model(X, model)
% The total likelihood of the model is the 
% First compute the likelihood of the training data given the current model
% parameters
log_lik = total_model_log_likelihood(X, model);
cont = 1; it = 1;
while (cont), 
    fprintf('iteration:%4d log_lik:%5.4f \n', it, log_lik);
    [expected_counts] = Estep(X, model);
    [model] = Mstep(expected_counts, X, model);
    log_lik_prev = log_lik;
    log_lik = total_model_log_likelihood(X, model);
    cont = (log_lik ~= 0) & (it<10) | (log_lik-log_lik_prev)>1e-5*abs(log_lik);
    it = it + 1; 
end;

function max_likelihood_clusters = get_max_clusters(X, model)

% Compute the max likelihood clusters for each observation
for i=1:size(X.obs, 2)
    best_likelihood = 0;
    best_chain = -1;
    probs = zeros(1, model.num_mcs);
    obs = X.obs(i).obs;
    for j=1:model.num_mcs
        chain_prob = chain_likelihood(obs, model.chain(j));
        probs(j) = chain_prob;
        if (best_chain == -1 | best_likelihood < chain_prob)
            best_likelihood = chain_prob;
            best_chain = j;
        end
    end
    max_likelihood_clusters(i).best_cluster = best_chain;
    max_likelihood_clusters(i).best_probs = best_likelihood;
    max_likelihood_clusters(i).probs = probs; 
end
% Keep going until the log likelihood does not improve much

% Model
%
% q: probabilities for each underlying mc, sums to 1
% chains: underlying Markov Chains
%
% Compute the expected counts, the \delta(z, i)
%
% expected_counts will be a k by n matrix, where k is the number of Markov
% Chains and n is the number of observations.
function [expected_counts] = Estep(X, model)

% Fill in the expected counts matrix
k = model.num_mcs;
n = size(X.obs, 2);
expected_counts = zeros(k, n);
for i=1:n
    % The total probability of this example
    obs = X.obs(i).obs;
    total_log_likelihood = model_log_likelihood(obs, model);
    for j=1:k
        chain_log = log(model.priors(j)) + chain_log_likelihood(obs, model.chain(j));
        expected_counts(j, i) = exp(chain_log - total_log_likelihood);
    end
    sum(expected_counts(:, i));
    % These should be the same
    %fprintf('%f %f\n', total_likelihood, model_likelihood(obs, model));
end

function [model] = Mstep(expected_counts, X, model)
% First refit the priors for each chain.  We can get this by summing over
% the entire matrix to get the denominator, and summing over each row to
% get the numerator.
denominator = sum(sum(expected_counts));
for i=1:model.num_mcs
    model.priors(i) = sum(expected_counts(i, :)) / denominator;
    [model.chain(i)] = optimize(model.chain(i), expected_counts(i, :), X);
end

% We also need to reoptimize the Markov Chains themselves based on the
% expected counts.

function [mc] = optimize(mc, expected_counts_row, X)

num_states = size(mc.t, 1);
assert(num_states == size(mc.T, 1));
% We have en end state
assert(num_states + 1 == size(mc.T, 2));
new_t = zeros(num_states, 1);
new_T = zeros(num_states, num_states + 1);
for i=1:size(X.obs, 2)
    obs = X.obs(i).obs;
    weight = expected_counts_row(i);
    obs_size = size(obs, 2);
    for j=1:obs_size
        if (j ~= 1)
            new_T(obs(j-1), obs(j)) = new_T(obs(j-1), obs(j)) + weight;
        else
            new_t(obs(j)) = new_t(obs(j)) + weight;
        end
        % If this is the end state, then we need to add weight to p(end |
        % obs(j)).
        if (j == obs_size)
            new_T(obs(j), num_states + 1) = new_T(obs(j), num_states + 1) + weight;
        end
    end
end
mc.t = new_t / sum(new_t);
for i=1:num_states
    % If we did not see some transitions, then just set this row to be
    % uniform
    denom = sum(new_T(i, :));
    if (denom == 0)
        new_T(i, :) = ones(1, num_states + 1);
    end
    mc.T(i, :) = new_T(i, :) / sum(new_T(i, :));
end

% Once the expected counts are computed, we can renormalize the parameters
% in the Mstep.
function [transformed] = transform(expected_counts, num_states)

for i=1:size(X.obs, 2)
    obs = X.obs(i).obs;
    state_counts = zeros(num_states, 1);
    for j=1:size(obs, 2) - 1
        transitions(obs(j), obs(j+1)) = transitions(obs(j), obs(j+1)) + 1;
    end
    for j=1:size(obs, 2)
        state_counts(obs(j)) = state_counts(obs(j)) + 1;
    end
    transformed(i).state_counts = state_counts;
    transformed(i).transitions = transitions;
end

function log_likelihood = total_model_log_likelihood(X, model)
n = size(X.obs, 2);
log_likelihood_vect = zeros(n, 1);
for i=1:size(X.obs, 2)
   log_likelihood_vect(i) = model_log_likelihood(X.obs(i).obs, model);
end
log_likelihood = sum(log_likelihood_vect);

% To compute the model likelihood for a single observation, we add together
% the probabilities of each of the underlying Markov Chains multiplied by
% their priors.
%function likelihood = model_likelihood(observation, model)
%likelihood = 0;
%likelihood_vect = zeros(model.num_mcs, 1);
%for i=1:model.num_mcs
%    likelihood_vect(i) = model.priors(i) * chain_likelihood(observation, model.chain(i));
%end
%likelihood_vect;
%likelihood = sum(likelihood_vect);

% The likelihood of the observation for a single Markov Chain
%function likelihood = chain_likelihood(observation, mc)
%likelihood = mc.t(observation(1));
%for i=2:size(observation, 2)
%    likelihood = likelihood * mc.T(observation(i-1), observation(i));
%end