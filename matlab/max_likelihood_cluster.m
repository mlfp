% To compute the model likelihood for a single observation, we add together
% the probabilities of each of the underlying Markov Chains multiplied by
% their priors.
function likelihood = model_likelihood(observation, model)
likelihood = 0;
likelihood_vect = zeros(model.num_mcs, 1);
for i=1:model.num_mcs
    likelihood_vect(i) = model.priors(i) * chain_likelihood(observation, model.chain(i));
end
likelihood_vect;
likelihood = sum(likelihood_vect);

% The likelihood of the observation for a single Markov Chain
%function likelihood = chain_likelihood(observation, mc)
%ikelihood = mc.t(observation(1));
%for i=2:size(observation, 2)
%    likelihood = likelihood * mc.T(observation(i-1), observation(i));
%end