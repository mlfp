function log_likelihood = total_model_log_likelihood(X, model)
n = size(X.obs, 2);
log_likelihood_vect = zeros(n, 1);
for i=1:size(X.obs, 2)
   log_likelihood_vect(i) = log(model_likelihood(X.obs(i).obs, model));
end
log_likelihood = sum(log_likelihood_vect);

% To compute the model likelihood for a single observation, we add together
% the probabilities of each of the underlying Markov Chains multiplied by
% their priors.
%function likelihood = model_likelihood(observation, model)
%likelihood = 0;
%likelihood_vect = zeros(model.num_mcs, 1);
%for i=1:model.num_mcs
%    likelihood_vect(i) = model.priors(i) * chain_likelihood(observation, model.chain(i));
%end
%likelihood_vect;
%likelihood = sum(likelihood_vect);

% The likelihood of the observation for a single Markov Chain
%function likelihood = chain_likelihood(observation, mc)
%likelihood = mc.t(observation(1));
%for i=2:size(observation, 2)
%    likelihood = likelihood * mc.T(observation(i-1), observation(i));
%end