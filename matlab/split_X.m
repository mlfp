function [train test] = split_X(X, prob)

train.varmap = X.varmap;
test.varmap = X.varmap;

next_train = 1;
next_test = 1;
for i=1:size(X.obs, 2)
    if rand(1, 1) < prob
        train.obs(next_train) = X.obs(i);
        next_train = next_train + 1;
    else
        test.obs(next_test) = X.obs(i);
        next_test = next_test + 1;
    end
end    