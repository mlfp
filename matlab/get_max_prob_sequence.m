function [best_sequence likelihood mat] = select_zero_prob_transitions(chain, length, varmap)

% Given a Markov Chain and length, find the most probable sequence
% At each step, at index i, we store the max probability that results in
% index i (log likelihood)

states = size(chain.t, 1);

for i=1:states
    mat.likelihood(i, 1) = log(chain.t(i));
end

% Store a matrix of size state x length.
% Each entry gives: the max likelihood of ending in state s at time t.  We
% also store the previous state that allowed us to end up here.
for i=2:length
    for j=1:states
        %%% HACK: Do not allow init -> init state transitions %%%
        if (j == 1)
            mat.likelihood(j, i) = -inf;
            mat.prev_state(j, i) = j;
            continue;
        end
        max_likelihood = 0;
        best_state = 0;
        % For each state, find the max likelihood transition
        for k=1:states
            likelihood = mat.likelihood(k, i - 1) + log(chain.T(k, j));
            %fprintf('%f %f %f %f\n', mat.likelihood(k, i - 1), chain.T(k, j), likelihood, max_likelihood);
            if (best_state == 0 | likelihood > max_likelihood)
                max_likelihood = likelihood;
                best_state = k;
            end
        end
        assert(best_state ~= 0);
        mat.likelihood(j, i) = max_likelihood;
        mat.prev_state(j, i) = best_state;
    end
end

% Now retrieve the max likelihood sequence
max_end_state = 0;
max_likelihood = 0;
for i=1:states
    likelihood = mat.likelihood(i, length);
    if (max_end_state == 0 | likelihood > max_likelihood) 
        max_end_state = i;
        max_likelihood = likelihood;
    end
end

% Now get the actual sequence that results in the max likelihood
best_sequence = zeros(length, 1);
best_sequence(length) = max_end_state;
cur_state = max_end_state;
for i=length:-1:2
    prev_state = mat.prev_state(cur_state, i);
    best_sequence(i - 1) = prev_state;
    cur_state = prev_state;
end
likelihood = max_likelihood;

% Convert the best sequence into a more readable format
for i=1:length
    state = best_sequence(i);
    readable_best_sequence{i, 1} = sprintf('%d %s', state, varmap{state, 1});
end
best_sequence = readable_best_sequence;
    