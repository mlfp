% The likelihood of the observation for a single Markov Chain
function likelihood = chain_likelihood(observation, mc)
likelihood = mc.t(observation(1));
for i=2:size(observation, 2)
    likelihood = likelihood * mc.T(observation(i-1), observation(i));
end