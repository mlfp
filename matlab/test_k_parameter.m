% Generate sample sequences from the model given.
function sequences = test_k_parameter(train, test, min_k, max_k)

index = 1;
for i=min_k:max_k
    [model c] = em_kmc(train, i);
    sequences{index}.model = model;
    sequences{index}.log_likelihood = total_model_log_likelihood(train, model);
    sequences{index}.test_log_likelihood = total_model_log_likelihood(test, model);
    index  = index + 1;
end