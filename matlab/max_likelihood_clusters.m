function max_likelihood_clusters = get_max_clusters(X, model)

% Compute the max likelihood clusters for each observation
for i=1:size(X.obs, 2)
    best_likelihood = 0;
    best_chain = -1;
    probs = zeros(1, model.num_mcs);
    obs = X.obs(i).obs;
    for j=1:model.num_mcs
        chain_prob = chain_likelihood(obs, model.chain(j));
        probs(j) = chain_prob;
        if (best_chain == -1 | best_likelihood < chain_prob)
            best_likelihood = chain_prob;
            best_chain = j;
        end
    end
    max_likelihood_clusters(i).best_cluster = best_chain;
    max_likelihood_clusters(i).best_probs = best_likelihood;
    max_likelihood_clusters(i).probs = probs; 
end