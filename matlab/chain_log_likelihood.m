% The likelihood of the observation for a single Markov Chain
function likelihood = chain_log_likelihood(observation, mc)
likelihood = log(mc.t(observation(1)));
n = size(observation, 2);
for i=2:n
    likelihood = likelihood + log(mc.T(observation(i-1), observation(i)));
end

end_state = size(mc.T, 2);
% Add in the stop probability
likelihood = likelihood + log(mc.T(observation(n), end_state));