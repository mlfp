function X = load_traces(filename)

% Load the traces from the file
% Skip over the first line
% For each subsequent line, load the observation and the name of the obs
% Store this in X(i).obs and X(i).name
f = fopen(filename);

index = 1;
% The first line gives the mapping of the identifiers
% Load the mapping of the identifiers (index --> name)
line = fgetl(f);
while 1
    [t line] = strtok(line);
    [name index] = strread(t, '%s%d', 'delimiter', ':');
    X.varmap{index, 1} = char(name);
    if (size(line, 2) == 0), break, end;
end
last = size(X.varmap, 1);
X.varmap{last + 1, 1} = 'END';

index = 1;
max_observations = 100;
% Now keep reading pairs of lines until the end of the file
while 1
    tline = fgetl(f);
    if ~ischar(tline), break, end; 
    obs = strread(tline);
    X.obs(index).obs = obs;
    if (size(obs, 2) > max_observations)
        X.obs(index).obs = obs(1:max_observations);
    end
    tline = fgetl(f);
    assert(ischar(tline));
    X.obs(index).name = tline;
    index = index + 1;
end;
    