function [zero_transitions] = zero_prob_transitions(model, varmap)

% Load the traces from the file
% Skip over the first line
% For each subsequent line, load the observation and the name of the obs
% Store this in X(i).obs and X(i).name

% Add up all of the transitions matrices, and then return the entries that
% are still 0.

transitions = zeros(model.num_states, model.num_states + 1);
for i=1:model.num_mcs
    transitions = model.chain(i).T + transitions;
end
[row col] = find(transitions == 0);

% Convert row col into a vector of strings
for i=1:size(row, 1)
    from = char(varmap{row(i), 1});
    to = char(varmap{col(i), 1});
    zero_transitions{i, 1} = char(sprintf('%s:%s', from, to));
end