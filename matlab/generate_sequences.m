% Generate sample sequences from the model given.
function sequences = generate_sequences(model, num_sequences, sequence_length)

for i=1:num_sequences
    sequences(i).obs = generate_sequence_from_model(model, sequence_length);
end

function sequence = generate_sequence_from_model(model, sequence_length)

% Figure out which MC we want to use
which_mc = get_random_index(model.priors');
sequence = generate_sequence_from_mc(model.chain(which_mc), sequence_length);

function sequence = generate_sequence_from_mc(mc, sequence_length)

sequence = zeros(1, sequence_length);

% First generate the initial state
sequence(1) = get_random_index(mc.t');
% Now generate the next states based on the transition matrices
for i=2:sequence_length
    sequence(i) = get_random_index(mc.T(sequence(i-1), :));
end

% Returns a random index, assuming that the passed in vector is a 1 x n
% vector whose contents sum to 1.
function index = get_random_index(dist)
assert(abs(sum(dist) - 1) < 0.001);
sequence_rand = rand(1);
current_min = 0;
index = -1;
for i=1:size(dist, 2)
    current_max = current_min + dist(i);
    if (sequence_rand >= current_min & sequence_rand < current_max)
        index = i;
        break;
    end
    current_min = current_max;
end
assert(index > 0 & index <= size(dist, 2))
