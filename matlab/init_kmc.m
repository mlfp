% Initializes the model to a random initialization point
% Takes in the number of states in the model and the number of underlying
% Markov Chains
function model = init_kmc(num_states, num_mcs)

model.num_states = num_states;
model.num_mcs = num_mcs;
% Initialize num_mcs Markov Chains, and then initialize the priors
for i=1:num_mcs
    model.chain(i) = init_single_mc(num_states);
end
model.priors = rand(num_mcs, 1);
model.priors = model.priors / sum(model.priors);

function mc = init_single_mc(num_states)

% Initialize a single Markov Chain w/ the number of states specified.  The
% initial probabilities must sum to 1, and each row in the transition
% matrix must sum to 1.

mc.t = rand(num_states, 1);

% Normalize the values to 1
mc.t = mc.t / sum(mc.t);
% Need one extra dimension for the end state
mc.T = rand(num_states, num_states + 1);

% Normalize each row to 1
for i=1:num_states
    mc.T(i, :) = mc.T(i, :) / sum(mc.T(i, :));
end