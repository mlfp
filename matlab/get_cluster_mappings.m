function get_cluster_mappings(X, k_min, k_max, fileprefix)

for i=k_min:k_max
    [model c] = em_kmc(X, i);
    filename = sprintf('%s-%d', fileprefix, i);
    f = fopen(filename, 'w');
    % Write out the name of the file and the training example
    for i=1:size(c, 2)
        fprintf(f, '%s,%d,%f\n', X.obs(i).name, c(i).best_cluster, model.priors(c(i).best_cluster));
    end
end    