% To compute log likelihood of the model for the given observation
% To prevent underflow, we need to do some tricks

function likelihood = model_log_likelihood(observation, model)
likelihood = 0;
likelihood_vect = zeros(model.num_mcs, 1);
for i=1:model.num_mcs
    likelihood_vect(i) = log(model.priors(i)) + chain_log_likelihood(observation, model.chain(i));
end
likelihood_vect;

% Get the max element in this vector
max_chain_log_likelihood = max(likelihood_vect);

% Now subtract this max likelihood from each log_likelihood
% Now take each the exp of each element.  Sum over the vector, take the
% log, and add the max_chain_log_likelihood.
likelihood_vect = exp(likelihood_vect - max_chain_log_likelihood);
likelihood = max_chain_log_likelihood + log(sum(likelihood_vect));

% The likelihood of the observation for a single Markov Chain
%function likelihood = chain_likelihood(observation, mc)
%ikelihood = mc.t(observation(1));
%for i=2:size(observation, 2)
%    likelihood = likelihood * mc.T(observation(i-1), observation(i));
%end