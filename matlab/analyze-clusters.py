import os
import re
import sys

class ClusterInfo:
    def __init__(self):
        self.priors = {}
        pass
    def HandleCluster(self, cluster, prior):
        self.priors[cluster] = prior
    def Prior(self, cluster):
        return self.priors[cluster]

class ProgramClusters:
    def __init__(self):
        self.clusters = {}
        self.total = 0

    def AddCluster(self, cluster):
        if not self.clusters.has_key(cluster):
            self.clusters[cluster] = 0
        self.clusters[cluster] += 1
        self.total += 1

    def Summary(self):
        # Figure out the best cluster, and its count
        best_cluster = -1
        best_count = 0
        for cluster in self.clusters.keys():
            num_clusters = self.clusters[cluster]
            if num_clusters > best_count:
                best_count = num_clusters
                best_cluster = cluster
        return (best_cluster, best_count, self.total)
        
def main(argv):
    infile = open(argv[1], 'r')
    program_clusters_map = {}
    cluster_info = ClusterInfo()
    clusters_re = re.compile('[^_\\d\\.]+')
    for line in infile:
        fields = line.strip().split(',')
        base = os.path.basename(fields[0])
        m = clusters_re.search(base)
        program_id = m.group(0)
        if not program_clusters_map.has_key(program_id):
            program_clusters_map[program_id] = ProgramClusters()
        cluster = int(fields[1])
        prior = float(fields[-1])
        program_clusters_map[program_id].AddCluster(cluster)
        cluster_info.HandleCluster(cluster, prior)
    for program in program_clusters_map.keys():
        (best_cluster, count, total) = program_clusters_map[program].Summary()
        print '%s & %d & %d & %.2f & %.2f \\\\' % (
                program, best_cluster, count, count * 1.0 / total, cluster_info.Prior(best_cluster))

    
if __name__ == '__main__':
    main(sys.argv)
