\documentclass[11pt]{article}
\usepackage[margin=2cm]{geometry}
\usepackage{amsmath,amsthm,amssymb}
\usepackage{graphicx}
\usepackage{color}
\usepackage{url}
\usepackage{fancyhdr}

\pagestyle{fancy}

\newtheorem{lem}{Lemma}
\newtheorem*{claim}{Claim}
\newcommand{\ul}{\underline}
\newcommand{\dtds}{\frac{\partial \alpha_t}{\partial \alpha_s}}

\title{Learning Garden-Variety Library Interactions}
\author{\small John Lai and Jean Yang\\ \small 6.867 Final Project}
\date{{\small December 4, 2008}}

\begin{document}
\thispagestyle{empty}

\setcounter{page}{0}
\maketitle
%\pagebreak

\begin{quote}
\centering
\emph{If you have a garden and a library, you have everything you need.}  --Cicero
\end{quote}

\section{Introduction}
Researchers have been trying to learn the ``correct'' way of using API libraries.  Those working on program verification have tried to do this to separate correct uses from incorrect uses in order to help programmers create more bug-free programs.  Ammons et. al. have shown that it is possible to infer specifications about interaction with APIs from looking at program traces~\cite{Mining}.  Those working on code synthesis have used the sequences of possible API calls as a search space in which to generate correct code interacting with APIs.  Mandelin et. al. have worked on automatically generating call sequences that interacts with APIs to achieve the desired goals~\cite{Jungloid}.

These previous approaches have not specifically addressed the property that the correct way of interacting with a particular API may consist of different distinct modes of interaction.  To consider a simple example, in C++ \texttt{malloc} is paired with \texttt{free}, \texttt{new} is paired with \texttt{free}, and \texttt{new[]} is paired with \texttt{free[]}.  Trying to call \texttt{free[]} on an object allocated with \texttt{new} is incorrect.  The ideal program specification would take into account different modes of interacting with the data.  Similarly, knowing the likelihood of different sequences of API calls would reduce the code synthesizer's search space and likely make code synthesis more effective.

The goal of our project was to determine whether we can apply machine learning techniques to learning modes of interaction with API's and abstract data structures.  We wanted to apply machine learning techniques to 1) see how we can map interactions with API libraries to separate clusters and 2) see if we could learn something from what these clusters look like.  The main decisions we made in our experimental setup were regarding how to extract meaningful training data from API interactions, how to represent API information as useful feature vectors, and which classification techniques are most appropriate.  For this project, we
\begin{enumerate}
\item generated data from program traces of Java programs with selected Java standard libraries.
\item investigated classification using a mixture of $k$-Markov chains. 
\end{enumerate}
We discuss our experimental setup in Section~\ref{setup}, our implementation of $k$-Markov chains in Section~\ref{markov}, and our results for the \texttt{ArrayList} library in Section~\ref{results}.

\section{Experimental setup}
\label{setup}

Though Java and C++ both have sufficiently ``interesting'' libraries and a large code base, we favored Java over C++ because 1) Java code is more portable, since it compiles to bytecode rather than machine code and is dynamically linked, 2) there are code copying issues involved with C++ templates, and 3) for hisorical reasons, there is more open source Java code that uses Java standard libraries than there is open source C++ code that uses the STL~\footnote{Many open source software in C++ uses its own version of abstract data structure libraries because 1) the libraries were not standardized across compilers and 2) people have efficiency issues with using the STL.}.  Here we describe how we implemented our profiler and constructed traces.

\subsection{Instrumenting Java code to produce traces}
We constructed runtime profiles of the code we run in order to construct the method traces (rather than adding logging code to the standard libraries) because 1) this method is more general, giving us the freedom of being able to construct learning data from any Java \texttt{jar} file and 2) this method gave us the freedom to choose our set of libraries to classify after seeing which ones were being used in the code.

We did this by writing a Java profiler that inserts logging instructions in the Java bytecode whenever there is a call to a standard library method of interest. Because Java's standard libraries are loaded in the bootstrap classloader rather than in the system loader, however, this causes problems for directly instrumenting the standard library classes.  Because of this, we instrument other classes loaded in the system classloader and, from code in those classes, log calls to standard library objects~\footnote{Because of these unforeseen issues involving Java's idiosyncracies, this process required far more work than initially anticipated.  We did, however, emerge victorious: we can now construct a profile of any Java JAR executable file.}.

We used the Java compiler's \texttt{javaagent} support for attaching a profiler to the execution of a program.  We build our profiler from the source code from the Java Interactive Profiler~\cite{JIP}, a Java profiler written in Java and built on top of the ASM bytecode reengineering library~\cite{ASM}.  Our program dynamically inserts bytecode instructions into the profiled code that then call our profiler functions that log calls to standard library functions.  We initially tracked sequences of calls to given libraries, but from analyzing preliminary results we realized that this information is not as useful as logging calls to specific instances of classes.  To do this, we inserted bytecode instructions that inspect the runtime stack to get at the object.  We take the hash value of each object in order to distinguish calls to different instances of the same class.

\subsection{Building traces}
We initially downloaded an assortment of programs, many of which had interactive interfaces, but we quickly realized that we would not be able to generate enough learning data by interacting with these programs.  We chose instead to focus on programs that 1) could take lots of inputs and therefore could provide enough data, 2) used enough common libraries with other programs we found, and 3) did not load libraries that cause our profiler to crash.  We were also limited by the fact that the profiler introduced significant overhead.

We generated traces on the following programs:
\begin{enumerate}
\item \textbf{Java Weather}, a library for parsing METAR fomatted weather data~\cite{Jweather}.  Our test program polled the San Francisco weather satellite for data at half-second intervals and parsed it.
\item \textbf{Lucene}, a library for searching webpages.  We acquired a base of 100 websites and ran each resulting page through the Lucene index builder.
\item \textbf{Mallet}, a machine learning language toolkit for processing webpages.  We ran code to import data from a set of webpages, train with it, and evaluate the results.
\end{enumerate}
We constructed about 100 profiles that contained object-level information for Java Weather and Lucene and took a few traces from Mallet for importing data, training a model, and evaluating the model.  We focused on the \texttt{ArrayList} library because it was the most extensively used in these programs.  While it would have been nicer to have a more systematic way of generating our data, this gave us a good amount of diverse data to work with.  Also, while this would cause our model to generalize poorly to new data from other programs, it still allows for the training of clusters and the priors in our mixture model can help account for the non-uniformly selected data.

\input{markov}

\section{Results}
\label{results}

\input{results}
\input{results_desc}
\input{conclusion}

\bibliographystyle{plain}
\bibliography{fpbib}
\end{document}
