\section{Classification Model}
\label{markov}
In this section we describe the $k$-Markov chains model, provide justification for why we chose it, and describe EM updates.

\subsection{$k$-Markov chains model}
Consider the standard Markov model defined by
\begin{eqnarray*}
p(x_1, \ldots, x_m) = t(x_1) \prod_{i=2}^m t(x_i | x_{i-1})
\end{eqnarray*}
where $t(x_1)$ is the probability that $x_1$ is the initial state and $t(x_i \vert x_{i-1})$ are the transition probabilities.

Similar to Gaussian Mixture Models, we can take a mixture of $k$ Markov chains.  A simple extension of the basic mixture model to allow variable length sequences gives the final model we used:
\begin{eqnarray*}
p(x_1, \ldots, x_m) = \sum_{z=1}^k q(z) \left(t_z(x_1) t(END | x_m) \prod_{i=2}^m t_z(x_i | x_{i-1})\right)
\end{eqnarray*}

\subsection{Model Choice}
We chose this model because it fits our goals of clustering different patterns of sequential interaction.  We chose to use $k$ Markov chains for the following reasons:
\begin{enumerate}
\item Mixture models are a natural way to uncover clusters in an unsupervised fashion by examining the component models of the mixture.
\item We wanted a model that would capture temporal information: the sequential information from the program logs are more interesting and informative than, say, the counts of certain method calls.  Markov chains are a good way of capturing this sequential information while keeping things simple by assuming that the system only cares about the previous state.  While program interaction is not solely determined by the previous function call (in fact, there may be add/remove sequence that have context-free structures), we reasoned that choosing a simpler model would make learning easier while still yielding insight into API interaction.  We also considered using support vector machines and capturing the temporal nature of our data by selecting pairs of function calls as features, but we felt that the mixture of Markov chains captured this in a more straightforward and intuitive fashion.
\end{enumerate}
We considered using hidden Markov models (HMMs) as well, but it was not clear to us what the hidden states would be.  Also, the learning problem would have been considerably more difficult if we chose to use a mixture of HMMs, as we would need to estimate both the clusters the points were chosen from as well as the hidden states in EM.

\subsection{Maximum Likelihood Estimation}
Similar to our derivation of the EM algorithm for other models, we first take a look at maximum likelihood estimation in the case where we observe exactly the underlying Markov Chain that our observations were drawn from.  In this case, similar to GMMs, the maximum likelihood estimates for the parameters in our model are simple.  Suppose that we have $n$ training sequence and define $\delta(z, i)$ to be 1 iff sample i was drawn from chain $z$.  The maximum likelihood estimates for our parameters will be:
\begin{eqnarray*}
n(z) & = & \sum_{i=1}^n \delta(z, i), \ \ q^*(z) =  \frac{n(z)}{n}\\
t_z^*(x) & = & \frac{\sum_{i=1}^n \delta(z, i) [[ x_{i1} == x]]}{\sum_{i=1}^n \delta(z, i)}\\
t_z^*(x_u | x_v) & =&  \frac{\sum_{i=1}^n \delta(z, i) \sum_{j=1}^{|\underline{x}_i| - 1} [[ x_{ij} == x_v \wedge x_{i,j+1} == x_u]]}{\sum_{i=1}^n \delta(z, i) \sum_{j=1}^{|\underline{x}_i|}[[ x_{ij} == x_v]]}\\
t_{z}^*(END|x_v) &= &\frac{\sum_{i=1}^n \delta(z, i) [[x_{i|\underline{x}_i|} == x_v]]}{\sum_{i=1}^n \delta(z, i) \sum_{j=1}^{|\underline{x}_i|}[[ x_{ij} == x_v]]}
\end{eqnarray*}
Note that since we have variable length sequences, we sum to $|x_i|$ in the denominator rather than $|x_i| - 1$.
Now that we have the estimates for the case where we know which Markov Chain each sequence was generated from, we can extend this to the case where we have a probability distribution over the Markov Chains that could have generated the sequence.  We can keep all of our estimates the same, except that we need to change the way the $\delta(z, i)$ are computed.  Rather than being strictly 0 or 1, $\delta(z, i)$ now represents a normalized probability that example $i$ was generated from the $z^{th}$ Markov Chain.  We compute $\delta(z, i)$ (assume that $\underline{x}_i$ has $m$ observations) with
\begin{eqnarray*}
\delta(z, i) = \frac{q(z) t_z(x_{i1}) t_z(END| x_{im}) \prod_{j=2}^{m} t(x_{ij} | x_{i,j-1})}{\sum_z q(z) t_z(x_{i1}) t_z(END | x_{im}) \prod_{j=2}^{m} t_z(x_{ij} | x_{i,j-1})}.
\end{eqnarray*}
We implemented both the representation of the $k$-Markov chains model and EM for this model in MATLAB without using any existing libraries or code.