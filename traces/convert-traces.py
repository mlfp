#!/usr/bin/python

import commands
import sys

class FunctionRepository:
    def __init__(self):
        self.function_to_index = {}
        self.next_index = 1
        self.functions = []
    def GetIndex(self, function_name):
        if not self.function_to_index.has_key(function_name):
            self.function_to_index[function_name] = self.next_index
            self.next_index += 1
            self.functions.append(function_name)
        return self.function_to_index[function_name]
    def ToString(self):
        # Print out a description of the form function_name:function_index
        ret_str = ''
        for i in range(len(self.functions)):
            if ret_str:
                ret_str += ' '
            ret_str += self.functions[i] + ':' + str(i + 1)
        return ret_str

class Sequence:
    def __init__(self, name):
        self.name = name
        self.observations = []
    def AddIndex(self, index):
        self.observations.append(index)
    def ToString(self):
        ret = ' '.join([str(obs) for obs in self.observations])
        ret += '\n' + self.name
        return ret

class Sequences:
    def __init__(self):
        self.sequences = []
        self.function_repos = FunctionRepository()
    def AddFile(self, filename):
        infile = open(filename, 'r')
        sequence = Sequence(filename)
        for line in infile:
            function = line.strip()
            index = self.function_repos.GetIndex(function)
            sequence.AddIndex(index)
        self.sequences.append(sequence)
    def Print(self):
        # Print the function repository
        print self.function_repos.ToString()
        # Print out each sequence
        for seq in self.sequences:
            print seq.ToString()

def GetFiles(pattern):
    (status, output) = commands.getstatusoutput('ls %s' % pattern)
    if not status:
        return output.split('\n')
    return None

def main(argv):
    # argv[1] gives a comma separated list of patterns to search for files.
    patterns = argv[1]
    files = []
    for pattern in patterns.split(','):
        matched_files = GetFiles(pattern)
        if matched_files and matched_files != []:
            files.extend(matched_files)
    sequences = Sequences()
    for filename in files:
        sequences.AddFile(filename)
    sequences.Print()

if __name__ == '__main__':
    main(sys.argv)
